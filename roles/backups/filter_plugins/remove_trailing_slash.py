
def remove_trailing_slash(path):
    if path.endswith('/'):
        return path[:-1]
    return path


class FilterModule(object):
    def filters(self):
        return dict(remove_trailing_slash=remove_trailing_slash)

