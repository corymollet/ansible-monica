# ansible-monica

**THIS IS STILL VERY MUCH A WORK-IN-PROGRESS**

Do not actually use this for any production environment yet.


Ansible playbooks for deploying [monica](https://github.com/monicahq/monica.git) on a Fedora server with [docker](https://docker.com) and docker-compose.

## Getting Started

You will need Ansible installed on your machine. Consult the [Ansible installation guide](https://docs.ansible.com/ansible/latest/installation_guide/index.html) for specifics.

## Configuration

There are two example inventory files, `production.inventory` and `staging.inventory`. Make copies of these files (without the `.inventory` extension) and edit one or both accordingly to point to the hosts (domain or IP addresses) of your staging and production servers. You can add or remove more inventory if needed (you don't have to use a staging server if you don't need one, obviously).

The `group_vars` and `host_vars` files contain more configuration variables. Copy the files ending in `.example`, removing that `.example` part, and edit accordingly.

The `monica` and `backups` roles have `vars/main.yml.example` files in them. You should repeat the same process (copying them to separate files without the `.example` extension and edit accordingly) for those role-specific variables as well.

## What's in the Playbook

The playbook will configure a Fedora Linux server to serve up Monica with docker-compose, using Nginx as a reverse proxy, and set up for [duplicity](http://duplicity.nongnu.org/) backups to an S3 protocl-compatible object storage service.

## Running the Playbook

TODO: more here

```sh
ansible-playbook -i production monica.yml
```

